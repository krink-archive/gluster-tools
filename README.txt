
#https://access.redhat.com/documentation/en-US/Red_Hat_Storage/3/html/Administration_Guide/index.html
#https://access.redhat.com/documentation/en-US/Red_Hat_Storage/2.0/html/Quick_Start_Guide/chap-Quick_Start_Guide-Virtual_Preparation.html
#https://access.redhat.com/documentation/en-US/Red_Hat_Storage/3/html/Administration_Guide/sect-Managing_Split-brain.html
#https://access.redhat.com/documentation/en-US/Red_Hat_Storage/2.0/html/Administration_Guide/sect-User_Guide-Monitor_Workload-Displaying_Volume_Status.html


backend bricks should have inode size=512
# mkfs.xfs -f -i size=512 -n size=8192 -d su=128K,sw=10 DEVICE to format the bricks to the supported XFS file system format. The inode size is set to 512 bytes to accommodate for the extended attributes used by Red Hat Storage. 

fstab
/dev/vdb1    /data  xfs defaults,inode64,noatime      1 2



